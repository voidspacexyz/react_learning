Setup this repository

1. Clone the code
2. Install NVM  (curl https://raw.githubusercontent.com/creationix/nvm/v0.25.0/install.sh | bash) . No need of sudo.
3. cd categorizor
4. nvm install v6.2.1
5. nvm use 6.2.1
6. npm install
7. npm run start

For deploying :
npm run build release

Deploy the build folder now to anywhere you would want


## Note : 
The base boiler plate is taken from : https://github.com/koistya/react-static-boilerplate

Retaining the same license on the boiler plate, Any code changes made on top of the boiler plate is thus releaed under GPL.v3

Also the data in this project are licensed under CC-With all the permissions you want, with an attribution back to this project.



